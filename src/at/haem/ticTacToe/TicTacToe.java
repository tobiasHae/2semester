package at.haem.ticTacToe;


public class TicTacToe {
	private String[][] gameArr = new String[3][3];
	private Boolean firsPlayer;
	private int setCounter; 
	private Boolean botBol;
	private Bot b1;
	
	public TicTacToe() {
		super();
		for(int col=0; col<3; col++) {
			for(int row=0; row<3; row++) {
				this.gameArr[row][col] = " ";
			}
		}
		this.firsPlayer = true;
		this.setCounter = 0;
		this.botBol = false;
	}
	
	public TicTacToe(Bot b1) {
		super();
		for(int col=0; col<3; col++) {
			for(int row=0; row<3; row++) {
				this.gameArr[row][col] = " ";
			}
		}
		this.firsPlayer = true;
		this.setCounter = 0;
		this.b1 = b1;
		this.botBol = true;
	}
	
	


	public String[][] getGameArr() {
		return gameArr;
	}

	public void showState() {
		for(int col=0; col<3; col++) {
			System.out.println(gameArr[0][col] + "|" + gameArr[1][col] +"|" + gameArr[2][col]);
		}
		
	}
	
	public void determineWinner(String str) {
		if(setCounter >= 5) {
			if(setCounter == 9) {
				System.out.println("It's a tie");
				System.exit(0);
				
			}
			for(int row =0; row<3; row++) {
				if(gameArr[row][0]==  gameArr[row][1] && gameArr[row][1] == gameArr[row][2]) {
					if (gameArr[row][0] == "x") {
						System.out.println("The winner is Player 1");
						showState();
						System.exit(0);
					}
					else {
						System.out.println("The winner is Player 2");
						showState();
						System.exit(0);
					}
				}

			}
			for(int col =0; col<3; col++) {
				if(gameArr[0][col]==  gameArr[1][col] && gameArr[1][col] == gameArr[2][col]) {
				if (gameArr[0][col] == "x") {
					System.out.println("The winner is Player 1");
					showState();
					System.exit(0);
				}
				else {
					System.out.println("The winner is Player 2");
					showState();
					System.exit(0);
				}
				}

			}
			
			if(gameArr[0][0]==  gameArr[1][1] && gameArr[1][1] == gameArr[2][2]) {
				if (gameArr[0][0] == "x") {
					System.out.println("The winner is Player 1");
					showState();
					System.exit(0);
				}
				else {
					System.out.println("The winner is Player 2");
					showState();
					System.exit(0);
				}
			}
			if(gameArr[0][2]==  gameArr[1][1] && gameArr[1][1] == gameArr[2][0]) {
				if (gameArr[0][0] == "x") {
					System.out.println("The winner is Player 1");
					showState();
					System.exit(0);
				}
				else {
					System.out.println("The winner is Player 2");
					showState();
					System.exit(0);
				}
			}
			
			
		}
		else {
			return ;
		}
	}
	
	public void add(String str) {
		String temp[] = str.split(",");
		int row = Integer.parseInt(temp[0]);
		int column = Integer.parseInt(temp[1]);
		if(this.botBol == false) {
		if(gameArr[column][row] != " ") {
			System.out.println("Feld schon besetzt!");
		}
		else {
			if(this.firsPlayer == true) {
				gameArr[column][row] = "x";
				this.firsPlayer = false;
			}
			else {
				gameArr[column][row] = "o";
				this.firsPlayer = true;
			}
			this.setCounter++;
			determineWinner(str);
		}
		showState();
		}
		else {
			if(gameArr[column][row] != " ") {
				System.out.println("Feld schon besetzt!");
			}
			else {
				
					gameArr[column][row] = "x";
					setCounter++;

					String botField = b1.getRandomField(this.gameArr);
					String botFieldArray[] = botField.split(",");
					int botRow = Integer.parseInt(botFieldArray[0]);
					int botCol = Integer.parseInt(botFieldArray[1]);
					this.gameArr[botCol][botRow] = "o";
					setCounter++;
					determineWinner(str);
			
			}
			showState();
		}
	}
}
