package at.haem.encryption;

public class CesarEncryption implements Encrypter{
	
	private int shift;
	
	
	
	
	public CesarEncryption(int shift) {
		super();
		this.shift = shift;
	}
	
	public CesarEncryption() {
		super();
		this.shift = 2;
	}

	@Override
	public String encrypt(String encryptString) {
		
		String result = "";
		for (int i=0; i< encryptString.length(); i++) {
			int temp = (int)encryptString.charAt(i);
			if(temp >= 65 && temp <= 90) {
				
				temp += this.shift;
				if(temp > 90) {
					while(temp > 90){
						int plus = temp -122;
						temp = 89 + plus;
					}
					
				}
				String c = Character.toString((char) temp);
				result += c;
			}
			else if (temp >= 97 && temp <= 122) {
				
				temp += this.shift;
				if(temp > 122) {
					while(temp > 122){
						int plus = temp -122;
						temp = 96 + plus;
					}
					
				}
				String c = Character.toString((char) temp);
				result += c;
			}
			else {
				System.out.println("wrong input");
				return "wrong input";
			}
		}
		System.out.println(result);
		return result;
	}

	@Override
	public String decrypt(String decryptString) {
		String result = "";
		
		for (int i=0; i< decryptString.length(); i++) {
			int temp = (int)decryptString.charAt(i);
			if(temp >= 65 && temp <= 90) {
				temp -= this.shift;
				if(temp < 65) {
					while(temp < 65) {
						int minus = 65 - temp ;
						temp = 91 - minus;
					}
					
				}
				String c = Character.toString((char) temp);
				result += c;
			}
			else if(temp >= 97 && temp <= 122) {
				temp -= this.shift;
				
				if(temp < 97) {
					while(temp < 97) {
						int minus = 97 - temp ;
						temp = 123 - minus;
					}
					
				}
				
				String c = Character.toString((char) temp);
				result += c;
			}
			else {
				System.out.println("wrong input");
				return "wrong input";
			}
			
			
		}
		System.out.println(result);
		return result;
	}
	
}
