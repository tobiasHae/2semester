package at.haem.encryption;

public interface Encrypter {
	public String encrypt(String encryptString);
	public String decrypt(String decryptString);
	
}
