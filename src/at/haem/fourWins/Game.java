package at.haem.fourWins;


public class Game {
	private String[][] gameArr;
	private Boolean firsPlayer;
	private int setCounter;
	
	public Game() {
		super();
		this.gameArr = new String[7][6]; 
		for(int col=0; col<7; col++) {
			for(int row=0; row<6; row++) {
				this.gameArr[col][row] = " ";
			}
		}
		this.firsPlayer = true;
		this.setCounter = 0;
	}
	

	public void showState() {
		for(int row=0; row<6; row++) {
			for(int col=0; col<7; col++) {
				if(col==0) {
					System.out.print("||");
				}
				
				System.out.print(gameArr[col][row] + "|");
				if(col==6) {
					System.out.print("|");
				}
			}
			System.out.println();
			
		}
	}
	
	
	
	public void add(int col){
		if(col >= 7) {
			System.out.println("Please choose a row from 0 to 6");
		}
		else {
			for(int row = 5; row>=0; row--) {
				if(gameArr[col][row] == " ") {
					this.setCounter++;
					if(this.firsPlayer == true) {
						gameArr[col][row] = "x";
						this.firsPlayer = false;
						determineWinner(col, row);
						break;
					}
					else {
						gameArr[col][row] = "o";
						this.firsPlayer = true;
						determineWinner(col, row);
						break;
					}
					
				}
				else if(row == 0 && gameArr[col][row] != " ") {
					System.out.println("This field is already full. Please make another choice!");
				}
				
			}
			
		}
		
		showState();
		
	}
	
	
	public void determineWinner(int col, int row) {
		if(this.setCounter >= 7) {
			//vertikale Abfrage
			if(row >= 0 && row <=2) {
				if(this.gameArr[col][row] == this.gameArr[col][row+1] && this.gameArr[col][row] == this.gameArr[col][row+2] && this.gameArr[col][row] == this.gameArr[col][row+3]) {
					System.out.println("The winner is Player: " + this.gameArr[col][row]);
					showState();
					System.exit(0);
				}
			}
			
			
			//horizontale Abfrage
			if(col <= 3) {
				if(this.gameArr[col][row] == this.gameArr[col+1][row]) {
					if(this.gameArr[col][row] == this.gameArr[col+2][row]) {
						if(this.gameArr[col][row] == this.gameArr[col+3][row]) {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
						else if(col-1 >=0) {
							if(this.gameArr[col][row] == this.gameArr[col-1][row]){
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
						
					}
					else if( col-2 >=0) {
						 if(this.gameArr[col][row] == this.gameArr[col-1][row]) {
							if(this.gameArr[col][row] == this.gameArr[col-2][row]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
				}
				else if(col-3 >= 0) {
					if(this.gameArr[col][row] == this.gameArr[col-1][row]) {
						if(this.gameArr[col][row] == this.gameArr[col-2][row]) {
							if(this.gameArr[col][row] == this.gameArr[col-3][row]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}

					}
				}
				
			}
			else if(col >=4) {
				if(this.gameArr[col][row] == this.gameArr[col-1][row]) {
					if(this.gameArr[col][row] == this.gameArr[col-2][row]) {
						if(this.gameArr[col][row] == this.gameArr[col-3][row]) {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
						else if(col+1 <=6) {
							if(this.gameArr[col][row] == this.gameArr[col+1][row]){
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
						
					}
					else if( col+2 <=6) {
						 if(this.gameArr[col][row] == this.gameArr[col+1][row]) {
							if(this.gameArr[col][row] == this.gameArr[col+2][row]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
				}
			}
			
			
			
			
			//schr�ge Abfrage
			if(col + 3 <= 6 && row +3 <=5) {
				if(this.gameArr[col][row] == this.gameArr[col+1][row+1]) {
					if(this.gameArr[col][row] == this.gameArr[col+2][row+2]) {
						if(this.gameArr[col][row] == this.gameArr[col+3][row+3]) {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
						else if (col-1 >= 0 && row-1 >= 0) {
							if(this.gameArr[col][row] == this.gameArr[col-1][row-1]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
					else if (col-2 >= 0 && row-2 >= 0) {
						if(this.gameArr[col][row] == this.gameArr[col-1][row-1]) {
							if(this.gameArr[col][row] == this.gameArr[col-2][row-2]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
				}
				
			}
			else if(col - 3 >= 0 && row -3 >= 0) {
				if(this.gameArr[col][row] == this.gameArr[col-1][row-1]) {
					if(this.gameArr[col][row] == this.gameArr[col-2][row-2]) {
						if(this.gameArr[col][row] == this.gameArr[col-3][row-3]) {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
						else if (col+1 <= 6 && row+1 <= 5) {
							if(this.gameArr[col][row] == this.gameArr[col+1][row+1]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
					else if (col+2 <= 6 && row+2 <= 5) {
						if(this.gameArr[col][row] == this.gameArr[col+1][row+1]) {
							if(this.gameArr[col][row] == this.gameArr[col+2][row+2]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
				}
			}
			else if(col + 2 <= 6 && row +2 <= 5 && col - 1 >= 0 && row -1 >= 0) {
				if(this.gameArr[col][row] == this.gameArr[col-1][row-1]) {
					if(this.gameArr[col][row] == this.gameArr[col+1][row+1]) {
						if(this.gameArr[col][row] == this.gameArr[col+2][row+2]) {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
					}
				}
			}
			
			else if(col + 1 <= 6 && row +1 <= 5 && col + 2 >= 0 && row -2 >= 0) {
				if(this.gameArr[col][row] == this.gameArr[col+1][row+1]) {
					if(this.gameArr[col][row] == this.gameArr[col-1][row-1]) {
						if(this.gameArr[col][row] == this.gameArr[col-2][row-2])  {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
					}
				}
			}
			
			
			
			
			//schr�ge Abfrage andere Richtung
			if(col + 3 <= 6 && row -3 >=0) {
				if(this.gameArr[col][row] == this.gameArr[col+1][row-1]) {
					if(this.gameArr[col][row] == this.gameArr[col+2][row-2]) {
						if(this.gameArr[col][row] == this.gameArr[col+3][row-3]) {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
						else if (col-1 >= 0 && row+1 <= 5) {
							if(this.gameArr[col][row] == this.gameArr[col-1][row+1]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
					else if (col-2 >= 0 && row+2 <= 5) {
						if(this.gameArr[col][row] == this.gameArr[col-1][row+1]) {
							if(this.gameArr[col][row] == this.gameArr[col-2][row+2]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
				}
				
			}
			else if(col - 3 >= 0 && row +3 <= 5) {
				if(this.gameArr[col][row] == this.gameArr[col-1][row+1]) {
					if(this.gameArr[col][row] == this.gameArr[col-2][row+2]) {
						if(this.gameArr[col][row] == this.gameArr[col-3][row+3]) {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
						else if (col+1 <= 6 && row-1 >= 0) {
							if(this.gameArr[col][row] == this.gameArr[col+1][row-1]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
					else if (col+2 <= 6 && row-2 >= 0) {
						if(this.gameArr[col][row] == this.gameArr[col+1][row-1]) {
							if(this.gameArr[col][row] == this.gameArr[col+2][row-2]) {
								System.out.println("The winner is Player: " + this.gameArr[col][row]);
								showState();
								System.exit(0);
							}
						}
					}
				}
			}
			else if(col - 2 >= 0 && row +2 <= 5 && col + 1 <= 6 && row -1 >= 0) {
				if(this.gameArr[col][row] == this.gameArr[col+1][row-1]) {
					if(this.gameArr[col][row] == this.gameArr[col-1][row+1]) {
						if(this.gameArr[col][row] == this.gameArr[col-2][row+2]) {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
					}
				}
			}
			
			else if(col - 1 >= 0 && row +1 <= 5 && col + 2 <= 6 && row -2 >= 0) {
				if(this.gameArr[col][row] == this.gameArr[col-1][row+1]) {
					if(this.gameArr[col][row] == this.gameArr[col+1][row-1]) {
						if(this.gameArr[col][row] == this.gameArr[col+2][row-2])  {
							System.out.println("The winner is Player: " + this.gameArr[col][row]);
							showState();
							System.exit(0);
						}
					}
				}
			}
			
			
			
			

			
		}
		
	}
	
}
