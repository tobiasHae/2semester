package at.haem.hangman;

import java.util.Random;

public class GameLogic {
	private String words[];
	private String word;
	private Random rand;
	private String displayString;
	private int failCounter;
	
	public GameLogic() {
		super();
		this.words = new String[3];
		this.words[0] = "HAMPELMANN";
		this.words[1] = "HAMPELFRAU";
		this.words[2] = "ZEBRA";
		this.rand = new Random(); 
        int rando = rand.nextInt(3); 
		this.word = words[rando];
		this.displayString = "";
		for(int i=0; i< this.word.length(); i++) {
			this.displayString += "*";
		}
		this.failCounter =0;
	}
	
	public void showState() {
		System.out.println(this.displayString);
	}
	
	public void checkInput(char input) {
		if(this.word.indexOf(input) >=0) {
			for(int i=0; i< this.displayString.length(); i++) {
				char temp = this.word.charAt(i);
				
				if( input == temp) {
					this.displayString = this.displayString.substring(0, i) + input + this.displayString.substring(i + 1);

				}
				
				
			}
			checkResult();
		}
		else {
			this.failCounter++;
		
			switch (this.failCounter)
	        {
	            case 1:
	            	System.out.println("Incorrect");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|_______________________");
	                break;
	            case 2:
	            	 System.out.println("Incorrect");
		               System.out.println("_________");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|_______________________");
	                break;
	            case 3:
	            	   System.out.println("Incorrect");
		               System.out.println("_________");
		               System.out.println("|       |");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|_______________________");
	                break;
	            case 4:
	            	   System.out.println("Incorrect");
		               System.out.println("_________");
		               System.out.println("|       |");
		               System.out.println("|   	O");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|_______________________");
	                break;
	            case 5:
	            	   System.out.println("Incorrect");
		               System.out.println("_________");
		               System.out.println("|       |");
		               System.out.println("|   	O");
		               System.out.println("|       |");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|_______________________");
	                break;
	            case 6:
	            	 	System.out.println("_________");
			            System.out.println("|       |");
			            System.out.println("|   	O");
			            System.out.println("|    ---|");
			            System.out.println("|");
			            System.out.println("|");
			            System.out.println("|_______________________");
	               
	                break;
	            case 7:
	            	   System.out.println("_________");
		               System.out.println("|       |");
		               System.out.println("|   	O");
		               System.out.println("|    ---|---");
		               System.out.println("|");
		               System.out.println("|");
		               System.out.println("|_______________________");
	                break;
	            case 8:
	            	   System.out.println("_________");
		               System.out.println("|       |");
		               System.out.println("|   	O");
		               System.out.println("|    ---|---");
		               System.out.println("|      /");
		               System.out.println("|     /");
		               System.out.println("|_______________________");
	                break;
	            case 9:
	            	   System.out.println("Incorrect");
		               System.out.println("_________");
		               System.out.println("|       |");
		               System.out.println("|   	O");
		               System.out.println("|    ---|---");
		               System.out.println("|      / \\");
		               System.out.println("|     /   \\");
		               System.out.println("|_______________________");
	               System.out.println("Sorry, you lost. The stick man is Dead! The secret word was: " + this.word);
	               System.exit(0);
	        }
		
		}
		
		
		System.out.println(this.displayString);
		
	}
	
	public void checkResult() {
		if(!(this.displayString.contains("*"))) {
			System.out.println("You got the word: "+ this.displayString + "!");
			System.exit(0);
		}
		
	
	
	}
}
