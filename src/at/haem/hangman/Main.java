package at.haem.hangman;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GameLogic g1 = new GameLogic();
		Scanner s = new Scanner(System.in);
		
		g1.showState();
		
		while(true) {
			char input = s.next().charAt(0);
			char upperInput = Character.toUpperCase(input);
			g1.checkInput(upperInput);
		}
		
	}

}
