package at.haem.bankomat;

import java.util.Scanner;

public class CashDispenser {
	private int balance;
	private Scanner s = new Scanner(System.in);
	
	public CashDispenser(int balance) {
		super();
		this.balance = balance;	
	}
	public CashDispenser() {
		super();
		this.balance = 500;	
	}
	
	public void showBalance() {
		System.out.println(this.balance);
	}
	public void deposit() {
		System.out.println("Please enter the amount of money you want to deposit");
		int deposit = this.s.nextInt();
		if(deposit>0) {
		this.balance += deposit;
		}
		else {
			System.out.println("You can't deposit a negative amount");
		}
	}
	
	public void payout() {
		System.out.println("Please enter the amount of money you want to payout");
		int payout = this.s.nextInt();
		if(payout > this.balance) {
			System.out.println("You can't go into the negative.");
		}
		else {
		this.balance -= payout;
		}
	}
	
	public void exit() {
		System.exit(0);
	}
}
